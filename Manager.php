<?php

namespace Modules\Manager;

use Symfony\Component\Console\Output\ConsoleOutput;
use Modules\Manager\Installer\Install;
use Modules\Manager\Installer\Remove;
use Modules\Manager\Repositories\ModuleRepositories;
use App\Repositories\MenuItems;
use App\Models\Role;
use App\Models\Permission;

class Manager
{
    protected $install;
    protected $remove;
    protected $console;

    public $repository;
    public $moduleName;
    public $data = [];

    # code...
    public function __construct()
    {
        $this->install = new Install($this);
        $this->remove = new Remove($this);
        $this->repository =  new ModuleRepositories;
        $this->console = new ConsoleOutput();

        $this->module('Manager');

        return $this;
    }

    public function __set($property, $value)
    {
        $this->data[$property] = $value;
    }

    public function __get($property)
    {
        if ($property == 'moduleName') {
            return $this->moduleName;
        } else {
            if (array_key_exists($property, $this->data)) {
                return $this->data[$property];
            } else {
                return null;
            }
        }
    }

    public function __call($method, $arguments)
    {
        if ($method == 'install') {
            return $this->install->run();
        }
        if ($method == 'remove') {
            return $this->remove->run();
        }
    }

    public function module($name)
    {
        if ($this->repository->findOrFail($name)) {
            $this->module = $this->repository->findOrFail($name);
            $this->moduleName = $name;
            return $this;
        }
    }

    public function reloadModule($name = "")
    {
        if (empty($name)) {
            $name = $this->moduleName;
        }

        return $this->module($name);
    }

    public function sendInfo($message)
    {
        return $this->console->writeln("<info>". $message ."</info>");
    }

    /**
     * Enabling Module Manager
     * @return boolean
     */
    public function enable()
    {
        $this->sendInfo("Enabling Module $this->moduleName..");
        // enable the module
        \Artisan::call("module:enable", [
            'module' => $this->moduleName
        ]);

        // run migration
        $this->sendInfo("Running migration for Module $this->moduleName..");
        \Artisan::call(
            "module:migrate-refresh",
            [
                'module' => "$this->moduleName"
            ]
        );

        // run seeder
        $this->sendInfo("Running seeder for Module $this->moduleName..");
        \Artisan::call("module:seed", [
            'module' => "$this->moduleName"
        ]);

        cache()->tags('menuTree')->flush();

        return true;
    }

    /**
     * Disabling Module Manager     * @return boolean
     */
    public function disable()
    {
        $this->sendInfo("Disabling Module $this->moduleName..");
        \Artisan::call("module:migrate-reset", [
            'module' => "$this->moduleName"
        ]);


        $this->sendInfo("Removing additional option for Module $this->moduleName..");
        $this->removeAdditionalPermission();

        \Artisan::call("module:disable", [
            'module' => "$this->moduleName"
        ]);

        cache()->tags('menuTree')->flush();

        return true;
    }

    public function removeAdditionalPermission()
    {
        $this->sendInfo("Removing Menus for Module Manager");
        // find Role
        $role = Role::find(1);

        // find permissions
        $permissions = Permission::where('name', 'like', "%$this->moduleName%")->get();

        // detach permissions from role
        $role->permissions()->detach($permissions);

        $this->removeAdditionalMenu($role);

        // delete permissions
        foreach ($permissions as $permission) {
            $permission->delete();
        }
    }

    public function removeAdditionalMenu($role)
    {
        // find menu
        $menuRepos = new MenuItems();
        $menu = $menuRepos->findByName("Module Manager");

        if ($menu) {
            // detach menu from role
            $role->menuitems()->detach($menu);

            // delete menu
            $menu->delete();
        }
    }
}
