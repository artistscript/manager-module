<?php

namespace Modules\Manager\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Module extends Model
{
    use CrudTrait, LogsActivity;
    protected $table = 'modules';
    protected $fillable = ['name', 'enabled'];
    protected $logAttributes = ['name', 'enabled'];
}
