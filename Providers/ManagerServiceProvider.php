<?php

namespace Modules\Manager\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Manager\Manager;
use Facades\Modules\Manager\Manager as ManagerFacades;

class ManagerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerCommand();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('manager', function () {
            return new Manager();
        });
    }

    protected function registerCommand()
    {
        \Artisan::command('manager-install', function () {
            if (ManagerFacades::module('Manager')->install()) {
                $this->comment("Module Manager is Installed!");
            }
        })->describe('Install and Enable Module Manager');

        \Artisan::command('manager-remove', function () {
            if (ManagerFacades::module('Manager')->remove()) {
                $this->comment("Module Manager is Removed!");
            }
        })->describe('Uninstall Module Manager');
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('manager.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php',
            'manager'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/Manager');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/Manager';
        }, \Config::get('view.paths')), [$sourcePath]), 'Manager');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/Manager');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'Manager');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'Manager');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['manager'];
    }
}
