<?php

namespace Modules\Manager\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\MenuItem;
use App\Models\Role;
use App\Models\Permission;
use Carbon\Carbon;

use Nwidart\Modules\Facades\Module;
use Modules\Manager\Models\Module as Mod;

class SeedBasicModTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $menu = MenuItem::create([
            "name" => "Module Manager",
            "type" => "internal_link",
            "link" => "modulemanager",
            "icon" => "fa-clone",
            "page_id" => null,
            "parent_id" => 1,
            "lft" => null,
            "rgt" => null,
            "depth" => null,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "deleted_at" => null
        ]);

        $role = Role::find(1);
        $role->menuitems()->attach($menu->id);

        $permission = Permission::create([
            'name' => 'ManagerCrudController@index',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'ManagerCrudController@create',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'ManagerCrudController@store',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'ManagerCrudController@show',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'ManagerCrudController@edit',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'ManagerCrudController@update',
        ]);
        $role->givePermissionTo($permission);


        $modules = Module::getByStatus(0);
        foreach ($modules as $module) {
            if ($module->getStudlyName() == 'Manager') {
                continue;
            }
            Mod::create([
                'name' => $module->getStudlyName(),
                'enabled' => 0
            ]);
        }
        $modules = Module::getByStatus(1);
        foreach ($modules as $module) {
            if ($module->getStudlyName() == 'Manager') {
                continue;
            }
            Mod::create([
                'name' => $module->getStudlyName(),
                'enabled' => 1
            ]);
        }
    }
}
