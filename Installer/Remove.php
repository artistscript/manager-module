<?php

namespace Modules\Manager\Installer;

use Modules\Manager\Exceptions\RemoveManagerException;
use Modules\Manager\Exceptions\ManagerException;
use Modules\Manager\Manager;

class Remove
{
    public $data;
    protected $parent;

    public function __construct(Manager $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function run()
    {
        $parent = $this->parent;

        // Reload module to get current state
        // This is mandatory for console use. Sometimes it will just stuck in tinker
        // because of the tinker always saves state
        $parent->reloadModule();

        if ($parent->moduleName == "Manager") {
            // throw_unless(app()->runningInConsole(), new RemoveManagerException);
            throw new RemoveManagerException;
        }

        throw_if($parent->module->disabled(), new ManagerException("Module $parent->moduleName is Not Enabled!"));

        $class = $this->getClass();

        if ($class::disable()) {
            if ($parent->moduleName != "Manager") {
                $data = $parent->repository->findByName($parent->moduleName);
                $data->enabled = 0;
                $data->save();
            }
            $parent->repository->flushTree();
            return true;
        }
    }

    protected function getClass()
    {
        $parent = $this->parent;
        $this->checkClass("Modules\\$parent->moduleName\\$parent->moduleName");
        return "\Facades\Modules\\$parent->moduleName\\$parent->moduleName";
    }

    protected function checkClass($class)
    {
        $parent = $this->parent;
        throw_unless(
            class_exists($class),
            new ManagerException("Module $parent->moduleName does not have valid uninstaller!")
        );
    }
}
