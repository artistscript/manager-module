<?php

namespace Modules\Manager\Installer;

use Modules\Manager\Exceptions\InstallManagerException;
use Modules\Manager\Exceptions\ManagerException;
use Modules\Manager\Exceptions\ModuleAlreadyEnabledException;
use Modules\Manager\Manager;

class Install
{
    public $data;
    protected $parent;

    public function __construct(Manager $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function run()
    {
        $parent = $this->parent;

        // Reload module to get current state
        // This is mandatory for console use. Sometimes it will just stuck in tinker
        // because of the tinker always saves state
        $parent->reloadModule();

        if ($parent->moduleName != "Manager") {
            $this->checkManager();
        }

        if ($parent->moduleName == "Manager") {
            throw_unless(app()->runningInConsole(), new InstallManagerException);
        }

        throw_if($parent->module->enabled(), new ModuleAlreadyEnabledException);

        $class = $this->getClass();

        if ($class::enable()) {
            if ($parent->moduleName != "Manager") {
                $data = $parent->repository->findByName($parent->moduleName);
                $data->enabled = 1;
                $data->save();
            }
            $parent->repository->flushTree();
            return true;
        }
    }

    /**
     * Checking Module Manager state
     * if the module manager is disabled, it will throw an error
     * @return [type] [description]
     */
    protected function checkManager()
    {
        if (\Module::find('Manager')->disabled()) {
            new ManagerException("Module Manager is Disabled. Please enable it first!");
        }
    }

    protected function getClass()
    {
        $parent = $this->parent;
        $this->checkClass("Modules\\$parent->moduleName\\$parent->moduleName");
        return "\Facades\Modules\\$parent->moduleName\\$parent->moduleName";
    }

    protected function checkClass($class)
    {
        $parent = $this->parent;
        throw_unless(
            class_exists($class),
            new ManagerException("Module $parent->moduleName does not have valid installer!")
        );
    }
}
