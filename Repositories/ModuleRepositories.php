<?php

namespace Modules\Manager\Repositories;

use Nwidart\Modules\Facades\Module;
use Modules\Manager\Models\Module as Mod;

class ModuleRepositories
{
    public $facade;
    public $model;

    public function __construct()
    {
        $this->facade = new Module;
        $this->model = new Mod;
        return $this;
    }

    public function __call($method, $arguments)
    {
        if ($method == 'findOrFail') {
            return Module::findOrFail(collect($arguments)->first());
        }
    }

    public function all()
    {
        $data = [];
        foreach ($this->getEnabledModules() as $enabled) {
            array_push($data, $enabled);
        }

        foreach ($this->getDisabledModules() as $disabled) {
            array_push($data, $disabled);
        }
        return collect($data)->sortBy('name');
    }

    public function getAndUpdate()
    {
        $this->generateDbRecord($this->getDisabledModules(), 0);
        $this->generateDbRecord($this->getEnabledModules(), 1);
    }

    public function findByName($name)
    {
        return Mod::whereName($name)->first();
    }

    public function generateDbRecord($modules, $status = 0)
    {
        foreach ($modules as $module) {
            if (is_null($this->findByName($module->getStudlyName()))) {
                if ($module->getStudlyName() != 'Manager') {
                    Mod::create([
                        'name' => $module->getStudlyName(),
                        'enabled' => $status
                    ]);
                }
            }
        }
    }

    private function getEnabledModules()
    {
        return Module::getByStatus(1);
    }

    private function getDisabledModules()
    {
        return Module::getByStatus(0);
    }

    public function flushTree()
    {
        return cache()->tags('menuTree')->flush();
    }
}
