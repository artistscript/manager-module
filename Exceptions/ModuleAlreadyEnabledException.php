<?php

namespace Modules\Manager\Exceptions;

use Exception;

class ModuleAlreadyEnabledException extends Exception
{
    protected $message = "Module Already Enabled!";
}
