<?php

namespace Modules\Manager\Exceptions;

use Exception;

class InstallManagerException extends Exception
{
    protected $message = 'Cannot Install Manager Module!';
}
