<?php

namespace Modules\Manager\Exceptions;

use Exception;

class ManagerException extends Exception
{
    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }
}
