<?php

namespace Modules\Manager\Exceptions;

use Exception;

class RemoveManagerException extends Exception
{
    protected $message = 'Cannot Remove Manager Module!';
}
