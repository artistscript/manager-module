<?php

namespace Modules\Manager\Http\Controllers;

use App\Http\Requests;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;

use Nwidart\Modules\Facades\Module;
use Modules\Manager\Models\Module as Mods;
use Modules\Manager\Repositories\ModuleRepositories;

use Facades\Modules\Manager\Manager;

class ManagerCrudController extends CrudController
{
    public function __construct(Mods $module)
    {
        $moduleRepo = new ModuleRepositories();
        $moduleRepo->getAndUpdate();

        parent::__construct();

        $this->crud->setModel('\Modules\Manager\Models\Module');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/modulemanager');
        $this->crud->setEntityNameStrings('module manager', 'module manager');
        $this->crud->denyAccess(['create', 'delete']);

        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => 'Module Name',
            ],[
                'name' => 'enabled',
                'label' => 'Enabled',
                'type' => 'check'
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => 'Module Name',
                'attributes' => [
                    'readonly' => 'true'
                ]
            ],[
                'name' => 'enabled',
                'label' => 'Enabled',
                'type' => 'checkbox'
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        $this->crud->hasAccessOrFail('update');
        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }
        if ($request->input('name') == 'Manager' && $request->input('enabled') == 0) {
            \Alert::error('Manager Cannot be Disabled!')->flash();
            return $this->performSaveAction();
        }

        if ($request->input('enabled') == 0) {
            $module = Manager::module($request->input('name'))->remove();
        }

        if ($request->input('enabled') == 1) {
            $module = Manager::module($request->input('name'))->install();
        }

        \Alert::success(trans('backpack::crud.update_success'))->flash();

        $this->setSaveAction();

        return $this->performSaveAction();
    }
}
