<?php

Route::group(['prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web','auth'], 'namespace' => 'Modules\Manager\Http\Controllers'], function () {
        CRUD::resource('modulemanager', 'ManagerCrudController');
    });
